import { FormBuilder } from '@angular/forms';
import { SettingsService } from './../settings.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  settings = {
    startDate: new Date().toDateString(),
    interval: 10
  }
  changeForm

  constructor(private settingsService: SettingsService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.changeForm = this.formBuilder.group({
      startDate: new Date(),
      interval: 0
    });

    this.settingsService.getSettings().then(settings => {
      this.settings = settings;
    })
  }

  onSubmitChange(data) {
    this.settingsService.changeSettings(data.startDate, data.interval).then(settings => {
      this.settings = settings;
    })
  }

}
