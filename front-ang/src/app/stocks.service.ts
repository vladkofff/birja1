import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StocksService {

  stocks = [{
    "distribution": "binomial",
    "maxValue": 10,
    "amount": 5,
    "initValue": 5,
    "name": "test"
}]

  constructor(private http: HttpClient) { }

  async getStocks() : Promise<any>{
    console.log(environment)
    return this.http.get(`${environment.backurl}/stocks`).toPromise();
  }
  async addStock(distribution, maxValue, amount, initValue, name): Promise<any> {
    return this.http.post(`${environment.backurl}/stocks`, {
      distribution, maxValue, amount, initValue, name
    }).toPromise();
  }
  async deleteStock(name) : Promise<any>{
    return this.http.post(`${environment.backurl}/stocks/delete`, {
      name
    }).toPromise();
  }
}
