import { SettingsComponent } from './settings/settings.component';
import { BrokersComponent } from './brokers/brokers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StocksComponent } from './stocks/stocks.component';


const routes: Routes = [
  {path: "", component: BrokersComponent},
  {path: "settings", component: SettingsComponent},
  {path: "stocks", component: StocksComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
