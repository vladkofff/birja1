import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BrokerService {

  brokers = [{
    name: "test",
    amount: 10
  }];

  constructor(private http: HttpClient) { }

  async getBrokers() : Promise<any> {
    return this.http.get(`${environment.backurl}/broker`).toPromise();
  }
  async addBroker(name, amount) : Promise<any> {
    return this.http.post(`${environment.backurl}/broker`, {
      name, amount
    }).toPromise()
  }
  async changeBroker(name, amount) : Promise<any> {
    return this.http.post(`${environment.backurl}/broker/change`, {
      name, amount
    }).toPromise()
  }
  async deleteBroker(name) : Promise<any> {
    return this.http.post(`${environment.backurl}/broker/delete`, {
      name
    }).toPromise()
  }
}
