import { BrokerService } from './../broker.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-brokers',
  templateUrl: './brokers.component.html',
  styleUrls: ['./brokers.component.css']
})
export class BrokersComponent implements OnInit {
  brokers;
  addForm;
  changeForm;
  deleteForm;

  constructor(private brokerService: BrokerService, private formBuilder: FormBuilder,) {
    this.addForm = this.formBuilder.group({
      name: '',
      amount: 0
    });

    this.changeForm = this.formBuilder.group({
      name: '',
      amount: 0
    });
    
    this.deleteForm = this.formBuilder.group({
      name: '',
    });
  }


  ngOnInit(): void {
    this.brokerService.getBrokers().then(brokers => {
      this.brokers = brokers;
    })

  }

  onSubmitAdd(data) {
    this.brokerService.addBroker(data.name, data.amount).then(brokers => {
      this.brokers = brokers
    })
  }

  onSubmitChange(data) {
    this.brokerService.changeBroker(data.name, data.amount).then(brokers => {
      this.brokers = brokers
    })
  }

  onSubmitDelete(data){
    this.brokerService.deleteBroker(data.name).then(brokers => {
      this.brokers = brokers
    })
  }

}
