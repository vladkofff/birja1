import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  settings = {
    startDate:"Sun Apr 12 2020",
    interval: 20
  }

  constructor(private http: HttpClient) { }

  async getSettings() : Promise<any> {
    return this.http.get(`${environment.backurl}/settings`).toPromise();
  }
  async changeSettings(startDate, interval) : Promise<any> {
    return this.http.post(`${environment.backurl}/settings`, {
      startDate: new Date(startDate).toDateString(), interval
    }).toPromise();
  }
}
