import { FormBuilder } from '@angular/forms';
import { StocksService } from './../stocks.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  stocks
  addForm
  deleteForm

  constructor(private stocksService: StocksService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.stocksService.getStocks().then(stocks => {
      this.stocks = stocks;
    })

    this.addForm = this.formBuilder.group({
      distribution: "", maxValue: 0, amount: 0, initValue: 0, name: ""
    });

    
    this.deleteForm = this.formBuilder.group({
      name: '',
    });
  }


  onSubmitAdd(data) {
    this.stocksService.addStock(data.distribution, data.maxValue, data.amount, data.initValue, data.name).then(stocks => {
      this.stocks = stocks
    })
  }

  onSubmitDelete(data){
    this.stocksService.deleteStock(data.name).then(stocks => {
      this.stocks = stocks
    })
  }
}
