const express = require('express')
const path = require('path');

const PORT = process.env.PORT || 4201
const bodyParser = require('body-parser')
const cors = require('cors')

const db = require("./db.json");

let brokers = db.brokers;
let stocks = db.stocks;
let settings = db.settings;

const getBrokers = () => Object.keys(brokers).map(key => ({name: key, ...brokers[key]}))
const getStocks = () => Object.keys(stocks).map(key => ({name: key, ...stocks[key]}))


express()
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(cors())
  .get("/api/broker", (req, res) => {
      res.json(getBrokers())
  })
  .post("/api/broker", (req, res) => {
        const {name, amount} = req.body;
    
        brokers[name] = {amount};
        res.json(getBrokers())
  })
  .post("/api/broker/change", (req, res) => {
      const {name, amount} = req.body;

      brokers[name].amount = amount;

      res.json(getBrokers());
  })
  .post("/api/broker/delete", (req, res) => {
    const {name} = req.body;

    delete brokers[name];

    res.json(getBrokers());
  })
  .get("/api/stocks", (req, res) => {
      res.json(getStocks());
  })
  .post("/api/stocks", (req, res) => {
    const {name, distribution, maxValue, amount, initValue} = req.body;

    stocks[name] = {distribution, maxValue, amount, initValue};

    res.json(getStocks());
  })
  .post("/api/stocks/delete", (req, res) => {
      const {name} = req.body;

      delete stocks[name];

      res.json(getStocks());
  })
  .post("/api/settings", (req, res) => {
    settings = {...req.body};
    res.json({...settings})
  })
  .get("/api/settings", (req, res) => res.json({...settings}))
  .use(express.static(__dirname + '/dist'))
  .get('/*', (req, res) => res.sendFile(path.join(__dirname, 'dist', 'index.html')))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))